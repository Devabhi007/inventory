from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.

class Box(models.Model):
    creator = models.ForeignKey(User,default=1,on_delete=models.CASCADE)
    length = models.DecimalField(max_digits=5,decimal_places=2)
    breadth = models.DecimalField(max_digits=5,decimal_places=2)
    height = models.DecimalField(max_digits=5,decimal_places=2)
    area = models.DecimalField(max_digits=50,decimal_places=2)
    volume = models.DecimalField(max_digits=50,decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    

    def save(self, *args, **kwargs):
        self.volume = self.length*self.breadth*self.height
        self.area =2*((self.length*self.breadth) + (self.breadth *self.height)+ (self.length*self.height))
        super(Box, self).save(*args, **kwargs)