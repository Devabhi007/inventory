from ast import Delete
from urllib import request
from rest_framework import serializers
from box.models import Box
from rest_framework.generics import ListAPIView
from rest_framework import viewsets
from rest_framework.authentication import BasicAuthentication,SessionAuthentication
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import OrderingFilter
from django.contrib.auth.models import User
# https://stackoverflow.com/questions/46491909/test-upload-excel-file-and-jsonfield-django-rest
class BoxSerializer(serializers.ModelSerializer):
    class Meta:
        model=Box
        fields = ['creator','length','breadth','height','area','volume','modified']
        read_only_fields=['area','volume','creator']

    def get_fields(self):
        # print(Box)
        fields= super().get_fields()
        user=self.context['request'].user
        print(user)
        if self.context['request'].user.is_staff==False:
            fields.pop('creator',None )
            fields.pop('modified',None)
        return fields


    def to_representation(self, instance):
        rep = super(BoxSerializer, self).to_representation(instance)
        if self.context['request'].user.is_staff:
            rep['creator'] = instance.creator.username
        return rep   
 
    def create(self, validated_data):
        print(validated_data)
        validated_data['creator'] = self.context['request'].user
        print(validated_data)
        obj = Box.objects.create(**validated_data)
        obj.save()
        return obj
    
    






























