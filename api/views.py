from dataclasses import fields
from pyexpat import model
import django
from django.http import QueryDict
from .serializers import BoxSerializer
from box.models import Box
from rest_framework.generics import ListAPIView
from rest_framework import viewsets,status
from rest_framework.authentication import BasicAuthentication,SessionAuthentication
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly,IsAdminUser,SAFE_METHODS
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import OrderingFilter
from django_filters import FilterSet,CharFilter
from django.db.models import Q
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.exceptions import APIException


class Filters(FilterSet):
    length_greater= CharFilter(field_name='length',method="filter",lookup_expr="(Please enter comma seperated range )")
    breadth = CharFilter(field_name='breadth',method="filter",lookup_expr="(Please enter comma seperated range )")
    height = CharFilter(field_name='height',method="filter",lookup_expr="(Please enter comma seperated range )")

    def filter(self,qs,name,value,*args,**kwargs):
        try:
            if name=="length":
                lowerValue, higherValue = value.split(",")
                Query = qs.filter(Q(length__gte=int(lowerValue)) &
                            Q(length__lte=int(higherValue)))
                return Query
            elif name == "breadth":
                lowerValue, higherValue = value.split(",")
                Query = qs.filter(Q(breadth__gte=int(lowerValue)) &
                            Q(breadth__lte=int(higherValue)))
                return Query

            elif name == "height":
                lowerValue, higherValue = value.split(",")
                Query = qs.filter(Q(height__gte=int(lowerValue)) &
                            Q(height__lte=int(higherValue)))
                return Query
            return None
        except Exception as e:
            raise APIException('Enter comma seperated two values')
    
    class Meta:
        model=Box
        fields=(
            'creator',
            'length_greater',
        )


class IsAdminUserOrReadOnly(IsAdminUser):

    def has_permission(self, request, view):
        is_admin = super(
            IsAdminUserOrReadOnly, 
            self).has_permission(request, view)
        return request.method in SAFE_METHODS or is_admin


class createset(viewsets.ModelViewSet):
    queryset = Box.objects.all()
    serializer_class = BoxSerializer
    # authentication_classes=[SessionAuthentication]
    # permission_classes=[IsAdminUserOrReadOnly]
    filter_class = Filters
    filter_backends = [DjangoFilterBackend]

    def destroy(self, request, *args, **kwargs):
        if self.get_object().creator == request.user:
            return super().destroy(request, *args, **kwargs)
        # return Response(status=status.HTTP_400_BAD_REQUEST)
        raise APIException("you don't have permission to delete box")